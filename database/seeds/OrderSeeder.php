<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Order::class, 12)->create()
            ->each(function ($order){
                $order->orderItems()->saveMany(
                    factory(\App\OrderItem::class, 3)
                        ->make(['order_id' => null])
                );
            });
    }
}
