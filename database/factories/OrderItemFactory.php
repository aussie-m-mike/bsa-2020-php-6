<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $product = \App\Product::all()->random();
    return [
        'quantity' => $faker->numberBetween(1, 11),
        'price' => $product->price,
        'discount' => $faker->numberBetween(0, 40),
        'product_id' => $product->id
    ];
});
