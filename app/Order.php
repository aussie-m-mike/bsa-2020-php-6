<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    public function getSumAttribute()
    {
        return $this->orderItems->sum('sum');
    }

}
