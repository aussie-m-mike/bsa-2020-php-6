<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    public function orders()
    {
        return $this->hasMany(Order::class, 'buyer_id', 'id');
    }

    public function getFullNameAttribute()
    {
        return $this->name.' '.$this->surname;
    }

    public function getFullAddressAttribute()
    {
        return $this->country.', '.$this->city.', '.$this->addressLine;
    }
}
