<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();

        return $orders->map(function ($order) {
            return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $buyer = Buyer::find($data['buyerId']);

        if ($buyer) {
            $order = new Order();
            $order->buyer()->associate($buyer);

            $items = $data['orderItems'];
            if ($items) {
                $orderItems = [];
                foreach ($items as $item) {

                    $product = Product::find($item['productId']);
                    if ($product) {
                        $orderItem = new OrderItem();
                        $orderItem->product()->associate($product);
                        $orderItem->price = $product->price;
                        $orderItem->quantity = $item['productQty'];
                        $orderItem->discount = $item['productDiscount'];

                        $orderItems[] = $orderItem;
                    } else {
                        return new Response([
                            'result' => 'fail',
                            'message' => 'Product with id ' . $item['productId'] . ' doesn\'t exist'
                        ]);
                    }
                }

                $order->save();
                $order->orderItems()->saveMany($orderItems);

                $order->refresh();

                return new OrderResource($order);
            }

        } else {
            return new Response([
                'result' => 'fail',
                'message' => 'Buyer doesn\'t exist'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        if ($order) {
            return new OrderResource($order);
        } else {
            return new Response([
                'result' => 'fail',
                'message' => 'Order not found'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $data = $request->all();

        if ($order) {
            $order->orderItems()->delete();

            $items = $data['orderItems'];
            if ($items) {
                $orderItems = [];
                foreach ($items as $item) {

                    $product = Product::find($item['productId']);
                    if ($product) {
                        $orderItem = new OrderItem();
                        $orderItem->product()->associate($product);
                        $orderItem->price = $product->price;
                        $orderItem->quantity = $item['productQty'];
                        $orderItem->discount = $item['productDiscount'];

                        $orderItems[] = $orderItem;
                    } else {
                        return new Response([
                            'result' => 'fail',
                            'message' => 'Product with id ' . $item['productId'] . ' doesn\'t exist'
                        ]);
                    }
                }
            }

            $order->orderItems()->saveMany($orderItems);
            $order->refresh();
            
            return new OrderResource($order);
        } else {
            return new Response([
                'result' => 'fail',
                'message' => 'Order not found'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
