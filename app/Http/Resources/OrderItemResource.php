<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'productName' => $this->product->name,
            'productQty' => $this->quantity,
            'productPrice' => round($this->product->price / 100, 2),
            'productDiscount' => $this->discount,
            'productSum' => round($this->sum / 100, 2)
        ];
    }
}
