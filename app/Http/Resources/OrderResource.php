<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId' => $this->id,
            'orderDate' => $this->created_at,
            'orderSum' => round($this->sum/100, 2),
            'orderItems' => $this->orderItems->map(function($orderItem){
                return new OrderItemResource($orderItem);
            }),
            'buyer' => new BuyerOrderResource($this->buyer),
        ];
    }
}
