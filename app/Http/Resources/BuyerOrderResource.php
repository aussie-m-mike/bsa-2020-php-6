<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BuyerOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'buyerFullName' => $this->fullName,
            'buyerAddress' => $this->fullAddress,
            'buyerPhone' => $this->phone
        ];
    }
}
